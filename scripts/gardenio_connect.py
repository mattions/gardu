#!/usr/bin/env python
import datetime
import rospy
import requests

from std_msgs.msg import Int16
from std_msgs.msg import Bool
from gardenio_msgs.msg import SoilSensor
import sensor_msgs

import sys

DATA_FILE = "/var/log/soil_hygro_reading.csv"
LED_ON_MILLISECONDS = 1000



GARDENIO_ROOT_URL = rospy.get_param('/GARDENIO_ROOT_URL')
GARDU_IDENTIFIER = rospy.get_param('/GARDU_IDENTIFIER')
    
led_publisher = rospy.Publisher('switch_led', Bool, queue_size=10)

def callback_soil_sensor(soil_sensor_msg, sensor_record_post_url="garden/sensor_record_add"):
    """
    Each of the sensor will send a single post request to gardenio, connected with 
    the GARDU identifier.
    
    :param soil_sensor_msg: SoilSensor Message with name of the sensor and analog read
    :param sensor_recordpost_url: Url to post on Gardenio
    """
    
    
    rospy.loginfo(rospy.get_caller_id() + " I heard %s", soil_sensor_msg)
    # Send the request to Gardenio
    # data accepted, keys:
    # data = {'gardu-identifier' : "MY_GARDU_IDENTIFIER", 
    #        "sensor" : '<Sensor_name>',
    #        "value" : '<analog_read>'}
    data = {'gardu-identifier' : GARDU_IDENTIFIER, 
           "sensor" : '{0}'.format(soil_sensor_msg.sensor_name),
           "value" : soil_sensor_msg.value}
    
    now = datetime.datetime.now()
    data['timestamp'] = now.isoformat()
#
#     with open(DATA_FILE, "a") as fh:
#         line = "{0}\t{1}\t{2}\t{3}\n".format(now.isoformat(), 
#                                              soil_sensor_msg.sensor_name, 
#                                              soil_sensor_msg.value,
#                                              sensor_to_plant[soil_sensor_msg.sensor_name])
#         fh.write(line)
    
    
    try:
        req = requests.post("{0}{1}".format(GARDENIO_ROOT_URL, sensor_record_post_url), data=data)
    except Exception as e:
        rospy.logerr("Couldn't post to Gardenio. Exception follow")
        rospy.logerr(e)

def callback_sensors_acquired(sensors_acquired, sensor_acquired_url="garden/all_sensors_acquired"):
    """
    When all the sensors have been read, this callback gets invoked.
    Check with Gardenio if any of the sensor is over the threshold, and in case 
    it is true, it publish on the led publisher to swithc on the led.
    """
    if sensors_acquired.data:
        ###
        # Send request to gardenio:
        data = {'gardu-identifier' : GARDU_IDENTIFIER, 
           "sensor_acquired" : sensors_acquired.data}
        
        #try:
        url = "{0}{1}".format(GARDENIO_ROOT_URL, sensor_acquired_url)
        req = requests.post(url, data=data)
        if req.status_code == 200:
            reqj = req.json()
            led_status = reqj['led_status']
            led_publisher.publish(led_status)
        else:
            rospy.logerr("Post failed. Response: {0}".format(req.json()))
                    
        #except Exception as e:
         #   msg = "Couldn't post to Gardenio using url: {0}. Exception: \n {1}".format(url, e)
          #
         # rospy.logerr(msg)

def main():
    rospy.init_node('gardenio_connect', anonymous=True)

    rospy.Subscriber("soil_sensor", SoilSensor, callback_soil_sensor)
    rospy.Subscriber("sensors_acquired", Bool, callback_sensors_acquired)
    rospy.loginfo("Listener to Soil Humidity channel configured.")
    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    # Setting up the publisher for the LED
    main()
