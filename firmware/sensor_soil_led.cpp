/*  GArdu

 Michele Mattioni
 May 10, 2018

 We are following this style: https://sites.google.com/a/cabrillo.edu/cs-11m/howtos/cppdoc
 */


#include <Arduino.h>
#include <stdio.h>
#include <string.h>
#include <Servo.h>

#include <ros.h>
#include <std_msgs/Int16.h>
#include <std_msgs/Empty.h>
#include <std_msgs/String.h>
#include <std_msgs/Bool.h>
#include <std_msgs/UInt16.h>

#include <gardenio_msgs/SoilSensor.h>

//ros::NodeHandle  nh;

// Tuning the nh for memory handling:
// 5 subscriber, 1 publisher, 200 buffer in, 150 out
// https://answers.ros.org/question/259687/arduino-to-rasspberry-pi3-rosserial-communication-issue/
ros::NodeHandle_<ArduinoHardware, 5, 2, 200, 200> nh;

// LED
#define LED_PIN 7


// SERVO
Servo servo; // servo to move over the tube splitter
#define SERVO_PIN 10

// PUMP
#define PUMP_PIN 9
//#define SOIL_MOISTURE_THRESHOLD 400
#define PUMP_MAX_OPENING_PUMP 10000 // millisecond

int sensor_moisture_reading = 0; //value for storing moisture value

typedef struct {
	char sensor_name[20];
	uint16_t soil_measure_analog_pin;
	uint16_t soil_power_pin;
	uint16_t pipe_servo_angle;
} sensorPipe;


#define SENSOR_PIPE_NUMBER 4
sensorPipe sensorsPipes[SENSOR_PIPE_NUMBER] = {
		// sensor_name, soil_measure_analog_pin, soil_power_pin, pipe_servo_angle
		{ "WhiteBlack", A5, 2, 36},
		{ "RedOrange", A4, 3, 72},
		{ "PurpleGray", A3, 4, 104},
		{ "RedBlack", A2, 5, 140},
};

// Toggle led
//void toggleLedCb( const std_msgs::Empty& toggle_msg){
//	digitalWrite(13, HIGH-digitalRead(13));   // blink the led
//}
//ros::Subscriber<std_msgs::Empty> toggle_led_sub("toggle_led", &toggleLedCb );


//
// Sensor reading
//
gardenio_msgs::SoilSensor soil_sensor_msg;
ros::Publisher soil_sensor("soil_sensor", &soil_sensor_msg);

//This is a function used to get the soil moisture content
int read_soil(sensorPipe sensor_pipe)
{
	//nh.loginfo(sensor_pipe.sensor_name);
	digitalWrite(sensor_pipe.soil_power_pin, HIGH);//turn Digital pin "On"
	delay(10);//wait 10 milliseconds
	sensor_moisture_reading = analogRead(sensor_pipe.soil_measure_analog_pin);//Read the SIG value from sensor
	digitalWrite(sensor_pipe.soil_power_pin, LOW);//turn Digital "Off"
	nh.spinOnce();
	return sensor_moisture_reading;
}



std_msgs::Bool acquired;
ros::Publisher sensors_acquired("sensors_acquired", &acquired);
/*
 * Acquire sensor humidity for all the defined sensors
 */
void acquire_soil_humidity_cb( const std_msgs::Empty& aquireSoilHumidity_msg){

	// Rotating on all the available sensor for acquisition
	for( int i = 0; i < SENSOR_PIPE_NUMBER; i = i + 1 ) {
		int sensor_moisture_reading = read_soil(sensorsPipes[i]);
		soil_sensor_msg.sensor_name = sensorsPipes[i].sensor_name;
		soil_sensor_msg.value = sensor_moisture_reading;
		soil_sensor.publish( &soil_sensor_msg);
	}
	acquired.data = true;
	sensors_acquired.publish(&acquired);

}

ros::Subscriber<std_msgs::Empty> acquire_soil_humidity_sub("acquire_soil_humidity", &acquire_soil_humidity_cb );

/*
 * Blink Led
 *
 */

void blink_led(const int blink_number){
	// blink on and off according to blink_number
	for (int i=0; i < blink_number; i++){
		digitalWrite(LED_PIN, HIGH);
		digitalWrite(13, HIGH);
		delay(500);
		nh.spinOnce();
		digitalWrite(LED_PIN, LOW);
		digitalWrite(13, LOW);
		delay(500);
	}
}

//
// PUMP Logic
//

/**
 * This is main function to invoke according to which plant we want to water
 */
void water_command_cb( const gardenio_msgs::SoilSensor& water_command){
	// parse the string
	const char* sensor_name = water_command.sensor_name;
	int soil_moisture_threshold =  water_command.value;
	sensorPipe sensor_pipe;
	boolean sensor_found = false;

    char log_msg[200];
    sprintf(log_msg, "Trying to Water Sensor: %s, tresh: %d", sensor_name, soil_moisture_threshold);
	nh.loginfo(log_msg);
	nh.spinOnce();
	digitalWrite(PUMP_PIN, HIGH);
	for( int i = 0; i < SENSOR_PIPE_NUMBER; i = i + 1 ) {
		if (strcmp ( sensorsPipes[i].sensor_name, sensor_name) == 0) {
			sensor_pipe = sensorsPipes[i];
			sensor_found = true;
			break;
		}
	}
	if (sensor_found){
		digitalWrite(LED_PIN, HIGH);
		// move servo to angle
		pinMode(SERVO_PIN, OUTPUT); // powering the pin of the servo

        servo.write(sensor_pipe.pipe_servo_angle); //set servo angle, should be from 0-180

//        sprintf(log_msg, "Name: %s, Angle: %d", (sensor_pipe.sensor_name, sensor_pipe.pipe_servo_angle));
//        nh.loginfo(log_msg);
        delay(400); //wait the servo to reach position
        nh.spinOnce();
        // Open the Pump
        digitalWrite(PUMP_PIN, HIGH);
		int soil_moisture_reading = read_soil(sensor_pipe);
		unsigned long start_time = millis();
		unsigned long time_open = 0;

		// The Lower Soil Moisture more water is present in the pot
		while(soil_moisture_reading > soil_moisture_threshold && time_open < PUMP_MAX_OPENING_PUMP){
			soil_moisture_reading = read_soil(sensor_pipe);
            nh.loginfo("Pump is ON");
            // check every 2000, spinning to keep communication with ROS master
			delay(2000);
			nh.spinOnce();
			unsigned long time = millis();
			time_open = time - start_time;
			sprintf(log_msg, "Time open calculated: %d", time_open);
			nh.loginfo(log_msg);
		}

		digitalWrite(PUMP_PIN, LOW); // Swith the Pump off
        pinMode(SERVO_PIN, INPUT); // switching the servo off
		digitalWrite(LED_PIN, LOW);
	}
}
ros::Subscriber<gardenio_msgs::SoilSensor> water_command_sub("water_command", &water_command_cb );

/// Low level debug function

//void servo_cb( const std_msgs::UInt16& cmd_msg){
//	pinMode(SERVO_PIN, OUTPUT); // powering the pin of the servo
//	servo.write(cmd_msg.data); //set servo angle, should be from 0-180
//	digitalWrite(13, HIGH-digitalRead(13));  //toggle led
//	delay(3000);
//	pinMode(SERVO_PIN, INPUT); // switching the servo off
//}
//ros::Subscriber<std_msgs::UInt16> servo_sub("servo", servo_cb);

//void led_cb( const std_msgs::Int16& led_duration){
//	blink_led(6);
//	nh.loginfo("Switch led ON for a duration");
//	digitalWrite(LED_PIN, HIGH);
//	digitalWrite(13, HIGH);
//	delay(led_duration.data);
//	digitalWrite(LED_PIN, LOW);
//	digitalWrite(13, LOW);
//}
//ros::Subscriber<std_msgs::Int16> led_sub("led", led_cb);

void switch_led_cb( const std_msgs::Bool& led_status){
	if (led_status.data == true) {
		nh.loginfo("Led ON");
		digitalWrite(LED_PIN, HIGH);
		digitalWrite(13, HIGH);
	} else {
		nh.loginfo("Led OFF");
		digitalWrite(LED_PIN, LOW);
		digitalWrite(13, LOW);
	}
}
ros::Subscriber<std_msgs::Bool> switch_led_sub("switch_led", switch_led_cb);

//void pump_cb( const std_msgs::UInt16& cmd_msg){
//	nh.loginfo("Switching pump and servo ON!!");
//	pinMode(SERVO_PIN, OUTPUT); // powering the pin of the servo
//	digitalWrite(PUMP_PIN, HIGH);
//	delay(cmd_msg.data);
//	digitalWrite(PUMP_PIN, LOW);
//	pinMode(SERVO_PIN, INPUT); // switching the servo off
//	nh.loginfo("Switching pump and servo OFF!!");
//}
//ros::Subscriber<std_msgs::UInt16> pump_sub("pump", pump_cb);

void setup()
{
	pinMode(13, OUTPUT);
	pinMode(LED_PIN, OUTPUT);

	// Rotating on all the available sensor for initialization
	for( int i = 0; i < SENSOR_PIPE_NUMBER; i = i + 1 ) {
		pinMode(sensorsPipes[i].soil_power_pin, OUTPUT);
	}

	// intialize the servo
	servo.attach(SERVO_PIN);
	// switch off servo to save battery
	pinMode(SERVO_PIN, INPUT);

	// initialize the pump
	pinMode(PUMP_PIN, OUTPUT);

	nh.initNode();
	nh.subscribe(acquire_soil_humidity_sub);
	nh.advertise(soil_sensor);
	nh.advertise(sensors_acquired);

//	nh.subscribe(servo_sub);
//	nh.subscribe(pump_sub);
	nh.subscribe(switch_led_sub);
//    nh.subscribe(water_command_sub);
}

void loop()
{
	nh.spinOnce();
	//delay(1);
}



