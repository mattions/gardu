#!/usr/bin/env python
import rospy
from gardenio_msgs.msg import SoilSensor

# sensorPipe sensorsPipes[SENSOR_PIPE_NUMBER] = {
#         // sensor_name, soil_measure_analog_pin, soil_power_pin, pipe_servo_angle
#         { "WhiteBlack", A5, 2, 36},
#         { "RedOrange", A4, 3, 72},
#         { "PurpleGray", A3, 4, 104},
#         { "RedBlack", A2, 5, 140},
# };


if __name__ == '__main__':
    water_publisher = rospy.Publisher('water_command', SoilSensor, queue_size=10)
    rospy.init_node('water_command_publisher_debug', anonymous=True)
    msg = SoilSensor()
    msg.sensor_name = "PurpleGray"
    msg.value = 600
    rospy.loginfo("Publishing: {0}".format(msg))
    water_publisher.publish(msg)